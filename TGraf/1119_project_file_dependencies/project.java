import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

public class project {

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		ArrayList<Integer>[] grafo;
		Scanner sc= new Scanner(System.in);
		int n = sc.nextInt();
		for (int i = 0; i < n; i++) {
			int x = sc.nextInt();
			int y = sc.nextInt();
			int[] depen= new int[x];
			for (int j = 0; j < depen.length; j++) {
				depen[j]=0;
			}
			grafo= new ArrayList[x];
			for (int j = 0; j < grafo.length; j++) {
				grafo[j]=new ArrayList<Integer>();
			}
			for (int j = 0; j < y; j++) {
				int fil = sc.nextInt()-1;
				int cont = sc.nextInt();
				for(int h=0;h<cont;h++){
					int dep=sc.nextInt()-1;
					depen[fil]++;
					grafo[dep].add(fil);
				}

			}
			PriorityQueue<Integer> pk=new PriorityQueue<Integer>();
			for (int j = 0; j < depen.length; j++) {
				if(depen[j]==0){
					pk.add(j);
				}
			}
			String res="";
			while(!pk.isEmpty()){
				int nu=pk.poll();
				res=res+(nu+1)+" ";
				ArrayList<Integer> es=grafo[nu];
				for (int j = 0; j < es.size(); j++) {
					int reg=es.get(j);
					depen[reg]--;
					if(depen[reg]==0){
						pk.add(reg);
					}
				}
			}
			System.out.println(res.trim());
			if(i!=n-1){
			System.out.println();}
		}
	}

}
