import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class Main {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int casos = Integer.parseInt(br.readLine());
		for (int i = 0; i < casos; i++) {
			HashMap<Integer,ArrayList<Integer>> grafo = new HashMap<Integer,ArrayList<Integer>>();
			boolean[] grado = new boolean[51];
			int arcos=Integer.parseInt(br.readLine());
			for (int j = 0; j < arcos; j++) {
				String[] arco = br.readLine().trim().split(" ");
				int n1=Integer.parseInt(arco[0]);
				int n2=Integer.parseInt(arco[1]);
				if(!grafo.containsKey(n1)){
					grafo.put(n1, new ArrayList<Integer>());
				}
				if(!grafo.containsKey(n2)){
					grafo.put(n2, new ArrayList<Integer>());
				}
				grado[n1]=!grado[n1];
				grado[n2]=!grado[n2];
				grafo.get(n1).add(n2);
				grafo.get(n2).add(n1);	
			}
			boolean a =false;
			for (int j = 0; j < grado.length; j++) {
				a|=grado[j];
			}
			a=!a;
			LinkedList<Integer> collar = new LinkedList<Integer>();
			while(a){
				a=armar(collar,grafo);
				if(collar.size()/2==arcos){
					break;
				}
			}
			if (a) {
				System.out.println("Case #"+(i+1));
				for (int j = 0; j < collar.size(); j=j+2) {
					System.out.println(collar.get(j)+" "+collar.get(j+1));
				}
			}else {
				System.out.println("Case #"+(i+1));
				System.out.println("some beads may be lost");
			}
			if(i!=casos-1){
				System.out.println();
			}
			
			
		}
	}

	private static boolean armar(LinkedList<Integer> collar, HashMap<Integer, ArrayList<Integer>> grafo) {
		boolean res=false;
		Set<Integer> gru = grafo.keySet();
		int poner=-1;
		int inicio = 0;
		if (collar.size() == 0) {
			for (Integer i : gru) {
				if (grafo.get(i).size() > 0) {
					res = true;
					inicio = i;
					break;
				}
			}
		} else {
			for (int i = 1; i < collar.size(); i++) {
				if(grafo.get(collar.get(i)).size() > 0){
					res = true;
					inicio = collar.get(i);
					poner=i;
					break;
				}
			}
		}
		if(res){
			LinkedList<Integer> collar2=new LinkedList<Integer>();
			int a=inicio;
			int b=grafo.get(a).get(0);
			grafo.get(a).remove((Object)b);
			grafo.get(b).remove((Object)a);
			collar2.add(a);
			collar2.add(b);
			int last=b;
			while(grafo.get(b).size()>0){
				a=b;
				b=grafo.get(a).get(0);
				grafo.get(a).remove((Object)b);
				grafo.get(b).remove((Object)a);
				collar2.add(a);
				collar2.add(b);
				last=b;
			}
			if(collar2.peekFirst()==last){
				res=true;
			}else{
				res=false;
			}
			if(collar.size()==0){
				collar.addAll(collar2);
			}else{
				collar.addAll(poner+1, collar2);
			}
		}
		return res;
	}

}
