import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Stack;

public class Rare {
	
	public static String res="";

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (br.ready()) {
			HashMap<Character,ArrayList<Character>> grafo= new HashMap();
			HashSet<Character> grupo=new HashSet();
			res="";
			
			String line1=br.readLine();
			String line2=br.readLine();
			while (!line2.equals("#")) {
				int in=0;
				int palmin=Math.min(line1.length(), line2.length());
				//System.out.println(in);
				while (in<palmin && line1.charAt(in)==line2.charAt(in)) {
					in++;
				}
				//System.out.println(in);
				if (in<palmin) {
					char ch1=line1.charAt(in);
					char ch2=line2.charAt(in);
					if(!grafo.containsKey(ch1)){
						grafo.put(ch1,new ArrayList());
					}
					if(!grafo.get(ch1).contains(ch2)){
						grafo.get(ch1).add(ch2);
					}
				}
				line1=line2;
				line2=br.readLine();
			}
			for ( Character d : grafo.keySet()) {
				if(!grupo.contains(d)){
					visit(grafo,grupo,d);
				}
			}
			System.out.println(res);
			
		}
	}

	private static void visit(HashMap<Character, ArrayList<Character>> grafo, HashSet<Character> grupo, Character d) {
		grupo.add(d);
		if(grafo.containsKey(d)){
		ArrayList<Character> es=grafo.get(d);
		for ( int i=0;i<es.size();i++) {
			if(!grupo.contains(es.get(i))){
				visit(grafo,grupo,es.get(i));
			}
		}
		}
		res=d+res;
	}


}
