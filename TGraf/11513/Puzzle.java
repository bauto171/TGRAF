
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class Puzzle {
	
	public static void main(String[] args) throws Exception {
		String base ="123456789";
		HashMap<String, String> mapa = new HashMap<String, String>();
		
		Queue<String> cola = new LinkedList<String>();
		cola.add(base);
		mapa.put(base, "");
		while (!cola.isEmpty()) {
			String c=cola.poll();
			String anterior=mapa.get(c);
			
			String r1 = Rotar(1,c);
			if(!mapa.containsKey(r1)){
				mapa.put(r1, "H1"+anterior);
				cola.add(r1);
			}
			String r2 = Rotar(2,c);
			if(!mapa.containsKey(r2)){
				mapa.put(r2, "H2"+anterior);
				cola.add(r2);
			}
			String r3 = Rotar(3,c);
			if(!mapa.containsKey(r3)){
				mapa.put(r3, "H3"+anterior);
				cola.add(r3);
			}
			String r4 = Rotar(4,c);
			if(!mapa.containsKey(r4)){
				mapa.put(r4, "V1"+anterior);
				cola.add(r4);
			}
			String r5 = Rotar(5,c);
			if(!mapa.containsKey(r5)){
				mapa.put(r5, "V2"+anterior);
				cola.add(r5);
			}
			String r6 = Rotar(6,c);
			if(!mapa.containsKey(r6)){
				mapa.put(r6, "V3"+anterior);
				cola.add(r6);
			}
			
	
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!br.ready()) {
			String a=br.readLine();
			if (a.equals("0")) {
				break;
			}
			String[] s1a=a.split(" ");
			String s1=s1a[0]+s1a[1]+s1a[2];
			s1a=br.readLine().split(" ");
			String s2=s1a[0]+s1a[1]+s1a[2];
			s1a=br.readLine().split(" ");
			String s3=s1a[0]+s1a[1]+s1a[2];
			String palabra=s1+s2+s3;
			String res = mapa.get(palabra);
			if(res!=null){
				System.out.println((res.length()/2)+" "+res);
			}else{
				System.out.println("Not solvable");
			}
		}
	}

	private static String Rotar(int i, String c) {
		String res="";
		char[] arch=c.toCharArray();
		if (i==1) {
			char tmp1=arch[1];
			char tmp2=arch[2];
			arch[2]=arch[0];
			arch[0]=tmp1;
			arch[1]=tmp2;
		}else if (i==2) {
			char tmp1=arch[4];
			char tmp2=arch[5];
			arch[5]=arch[3];
			arch[3]=tmp1;
			arch[4]=tmp2;
		}else if (i==3) {
			char tmp1=arch[7];
			char tmp2=arch[8];
			arch[8]=arch[6];
			arch[6]=tmp1;
			arch[7]=tmp2;
		}else if (i==4) {
			char tmp1=arch[0];
			char tmp2=arch[3];
			arch[0]=arch[6];
			arch[3]=tmp1;
			arch[6]=tmp2;
		}else if (i==5) {
			char tmp1=arch[1];
			char tmp2=arch[4];
			arch[1]=arch[7];
			arch[4]=tmp1;
			arch[7]=tmp2;
			
		}else if (i==6) {
			char tmp1=arch[2];
			char tmp2=arch[5];
			arch[2]=arch[8];
			arch[5]=tmp1;
			arch[8]=tmp2;
		}
		res=String.copyValueOf(arch);
		return res;
	}
}
