import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.StringTokenizer;
public class Bank {
	public static class Pair implements Comparable<Pair> {
		public int n2;
		public int v;
		public Pair(int nod2,int valor){
			n2=nod2;
			v=valor;
		}
		@Override
		public int compareTo(Pair o) {
			int res=0;
			if (v<o.v) {
				res=-1;
			}if (v>o.v) {
				res=1;
			}
			return res;
		}
	}
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter bw = new PrintWriter(new OutputStreamWriter(System.out));
		while (!br.ready()) {
			HashMap<Integer, ArrayList<Pair>> grafo= new HashMap<Integer, ArrayList<Pair>>(); 
			StringTokenizer st=new StringTokenizer(br.readLine());
			int v = Integer.parseInt(st.nextToken());
			int a = Integer.parseInt(st.nextToken());
			int b = Integer.parseInt(st.nextToken());
			int p = Integer.parseInt(st.nextToken());
			for (int i = 0; i < a; i++) {
				st=new StringTokenizer(br.readLine());
				int a1 = Integer.parseInt(st.nextToken());
				int a2 = Integer.parseInt(st.nextToken());
				int peso = Integer.parseInt(st.nextToken());
				if(!grafo.containsKey(a1)){
					grafo.put(a1, new ArrayList<Pair>());
				}
				if(!grafo.containsKey(a2)){
					grafo.put(a2, new ArrayList<Pair>());
				}
				grafo.get(a1).add(new Pair(a2,peso));
				grafo.get(a2).add(new Pair(a1,peso));
			}
			int[] bancos = new int[b];
			ArrayList<Integer> bans = new ArrayList<Integer>();
			st=new StringTokenizer(br.readLine());
			for (int i = 0; i < b; i++){
				bans.add(Integer.parseInt(st.nextToken()));
			}
			int[] policia = new int[p]; 
			if(p!=0){
				st=new StringTokenizer(br.readLine());
				for (int i = 0; i < p; i++){
					policia[i] = Integer.parseInt(st.nextToken());
				}
			}
			
			PriorityQueue<Pair> dj=new PriorityQueue<Pair>();
			int[] distancia=new int[v];
			for (int i = 0; i < distancia.length; i++) {
				distancia[i]=120000000;
			}
			for (int i = 0; i < policia.length; i++) {
				dj.add(new Pair(policia[i],0));
				distancia[policia[i]]=0;
			}
			while (!dj.isEmpty()) {
				Pair g = dj.poll();
				if (grafo.containsKey(g.n2)) {
					for (Pair f : grafo.get(g.n2)) {
						if (distancia[f.n2] > g.v + f.v) {
							// System.out.println("relajo");
							distancia[f.n2] = g.v + f.v;
							dj.add(new Pair(f.n2, distancia[f.n2]));
						}
					}
				}
			}
			int tmax=distancia[0];
			for (int i = 1; i < bancos.length; i++) {
				if(distancia[bancos[i]]>tmax){
					tmax=distancia[i];
				}
			}
			int c=0;
			String h="";
			Collections.sort(bans);
			for (int i = 0; i < bans.size(); i++) {
				if(distancia[bans.get(i)]==tmax){
					c++;
					h+=i+" ";
				}
			}
			h=h.trim();
			bw.println(c+" "+((tmax==120000000)?'*':""+tmax));
			bw.println(h);
		}
		bw.close();
		br.close();
	}
}