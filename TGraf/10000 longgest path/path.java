import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.StringTokenizer;


public class path{
	
	public static class Pair implements Comparable<Pair> {
		public int n2;
		public int v;
		public Pair(int nod2,int valor){
			n2=nod2;
			v=valor;
		}
		@Override
		public int compareTo(Pair o) {
			int res=0;
			if (v<o.v) {
				res=-1;
			}if (v>o.v) {
				res=1;
			}
			return res;
		}
	}

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st ;
		int n= Integer.parseInt(br.readLine());
		int caso=0;
		while(n!=0){
			caso++;
			HashMap<Integer, ArrayList<Integer>> grafo= new HashMap<Integer, ArrayList<Integer>>(); 
			int start = Integer.parseInt(br.readLine())-1;
			String line=br.readLine();
			while(!line.equals("0 0")) {
				st=new StringTokenizer(line);
				int a1=Integer.parseInt(st.nextToken())-1;
				int a2=Integer.parseInt(st.nextToken())-1;
				if(!grafo.containsKey(a1)){
					grafo.put(a1, new ArrayList<Integer>());
				}
				if(!grafo.containsKey(a2)){
					grafo.put(a2, new ArrayList<Integer>());
				}
				grafo.get(a1).add(a2);
				line=br.readLine();
			}
			int[] dist=new int[n];
			PriorityQueue<Pair> dj= new PriorityQueue<Pair>();
			for (int i = 0; i < n; i++) {
				dist[i]=Integer.MIN_VALUE;
			}
			dj.add(new Pair(start, 0));
			while (!dj.isEmpty()) {
				Pair g = dj.poll();
				if (grafo.containsKey(g.n2)) {
					for (int f : grafo.get(g.n2)) {
						if (dist[f] < g.v + 1) {
							// System.out.println("relajo");
							dist[f] = g.v + 1;
							dj.add(new Pair(f, dist[f]));
						}
					}
				}
			}
			int max=dist[0];
			int dm=0;
			for (int i = 1; i < dist.length; i++) {
				if(max<dist[i]){
					max=dist[i];
					dm=i;
				}
			}
			System.out.println("Case "+caso+": The longest path from "+(start+1)+" has length "+max+", finishing at "+(dm+1)+".");
			System.out.println();
			n=Integer.parseInt(br.readLine());
		}
	}
	
	

}
