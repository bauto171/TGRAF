import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;



public class FireStation {
	
	public static class Arco implements Comparable<Arco>{
		public int n2;
		public int v;
		public Arco(int nod2,int valor){
			n2=nod2;
			v=valor;
		}
		@Override
		public int compareTo(Arco o) {
			int res=0;
			if (v<o.v) {
				res=-1;
			}if (v>o.v) {
				res=1;
			}
			return res;
		}
		
		
	}

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n =Integer.parseInt(br.readLine());
		br.readLine();
		for(int caso=0;caso<n;caso++){
			
			String[] ia=br.readLine().split(" ");
			int nf=Integer.parseInt(ia[0]);
			int ni=Integer.parseInt(ia[1]);
			int[] fire=new int[nf];
			for (int i = 0; i < nf; i++) {
				fire[i]=(Integer.parseInt(br.readLine())-1);
			}
			String tmp=br.readLine();
			ArrayList<Arco>[] grafo = new ArrayList[ni];
			for (int i = 0; i < ni; i++) {
				grafo[i]=new ArrayList<Arco>();
			}
			while(tmp.trim().length()>0){
				ia=tmp.split(" ");
				int eg1=Integer.parseInt(ia[0])-1;
				int eg2=Integer.parseInt(ia[1])-1;
				int eg3=Integer.parseInt(ia[2]);
				grafo[eg1].add(new Arco(eg2,eg3));
				grafo[eg2].add(new Arco(eg1,eg3));
				if (br.ready()) {
					tmp=br.readLine();
				}else{
					tmp="";
				}
				
			}
			int est=-1;
			int maxtotal=Integer.MAX_VALUE;
			for(int j =0 ;j< ni;j++){
				int max=-1;
				PriorityQueue<Arco> dj=new PriorityQueue<Arco>();
				int[] distancia=new int[ni];
				for (int i = 0; i < distancia.length; i++) {
					distancia[i]=10000000;
				}
				
				boolean[] vi=new boolean[ni];
				for (int i = 0; i < fire.length; i++) {
					dj.add(new Arco(fire[i],0));
					distancia[fire[i]]=0;
				}
				dj.add(new Arco(j,0));
				distancia[j]=0;
				
				while(!dj.isEmpty()){
					Arco g=dj.poll();
					//System.out.println("saco "+g.n2+" con valor "+g.v);
					if(vi[g.n2]==false){
						vi[g.n2]=true;
						for (Arco f:grafo[g.n2]) {
							//System.out.println("uso "+f.n2+" con valor "+f.v);
							//System.out.println("con distancia "+distancia[f.n2]);
							if(distancia[f.n2]>g.v+f.v){
								//System.out.println("relajo");
								distancia[f.n2]=g.v+f.v;
								dj.add(new Arco(f.n2,distancia[f.n2]));
							}
						}
					}
				}
				//for (int i = 0; i < distancia.length; i++) {
				//	System.out.print(distancia[i]+" ");
				//}
				//System.out.println();
				for (int i = 0; i < distancia.length; i++) {
					if(distancia[i]>max){
						max=distancia[i];
					}
				}
				//System.out.println(max);
				if(maxtotal>max){
					maxtotal=max;
					est=j;
				}
			}
			System.out.println(est+1);
			if(caso!=n-1){
				System.out.println();
			}
		}
	}
}



