import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Two {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line=br.readLine();
		int caso=0;
		while(!line.equals("0")){
			caso++;
			HashMap<String,Integer> map=new HashMap<String, Integer>();
			StringTokenizer st = new StringTokenizer(line);
			int a= Integer.parseInt(st.nextToken());
			int[] car= new int[a];
			for (int i = 0; i <a; i++) {
				car[i]=Integer.parseInt(st.nextToken());
			}
			int diff= diferencia(0,a,car,map);
			System.out.println("In game "+caso+", the greedy strategy might lose by as many as "+diff+" points.");
			line=br.readLine();
		}
		
	}

	private static int diferencia(int i, int a, int[] car, HashMap<String, Integer> map) {
		int res=-100;
		if(map.containsKey((i+" "+a))){
			res=map.get((i+" "+a));
		}else{
			if(a-i==2){
				res= Math.abs(car[a-1]-car[i]);
			}else{
				int b=Math.max(car[a-1], car[i+1]);
				int c,d;
				if(b==car[i+1]){
					c=i+2;
					d=a;
				}else{
					c=i+1;
					d=a-1;
				}
				int p1 =car[i]-b+diferencia(c, d, car,map);
				b=Math.max(car[a-2], car[i]);
				if(b==car[i]){
					c=i+1;
					d=a-1;
				}else{
					c=i;
					d=a-2;
				}
				int p2 =car[a-1]-b+diferencia(c, d, car,map);
				res= Math.max(p1, p2);
			}
			map.put((i+" "+a), res);
		}
		return res;
	}

}
